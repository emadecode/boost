import { createAction, createReducer } from "@reduxjs/toolkit";
import { BoostCardModel } from "../../components/boost-card/BoostCard.models";

const ADD_NEW_CARD = "boost/card/ADD_NEW_CARD";

export const addNewCard = createAction<BoostCardModel>(ADD_NEW_CARD);

interface IStateModel {
  cardsList: Array<BoostCardModel>;
}
const initialState: IStateModel = {
  cardsList: [new BoostCardModel(), new BoostCardModel()],
};

export default createReducer<IStateModel>(initialState, (builder) => {
  builder.addCase(addNewCard, (state, action) => {
    state.cardsList.push(action.payload);
  });
});
