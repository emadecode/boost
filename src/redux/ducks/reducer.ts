import { combineReducers } from "@reduxjs/toolkit";
import card from "./card";

export default combineReducers({
  card,
});
