import React from "react";
import { render, screen, fireEvent } from "./core/lib/test-utils";

import { ThemeProvider } from "styled-components";
import { Provider as ReduxProvider } from "react-redux";
import App from "./App";
import { store } from "./redux/store";
import theme from "./styles/theme";

beforeEach(() => {
  render(
    <ReduxProvider store={store}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </ReduxProvider>
  );
});

describe("App", () => {
  it("should show two cards on page load", () => {
    const BoostCardEl = screen.getAllByTestId("boost-card");
    expect(BoostCardEl).toHaveLength(2);
  });
  it("should add new BoostCard on submit", async () => {
    const boostCardButtonEl = screen.getAllByTestId("boost-card-button")[0];

    fireEvent.click(boostCardButtonEl);

    const createBoostButtonEl = await screen.getAllByTestId(
      "create-boost-button"
    )[0];
    expect(createBoostButtonEl).toBeInTheDocument();

    fireEvent.click(createBoostButtonEl);

    const boostCardEl = await screen.findAllByTestId("boost-card");
    expect(boostCardEl).toHaveLength(3);
  });
});
