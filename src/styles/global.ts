import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }
  html {
    font-size: 65.5%;
    @media only screen and (min-width: 768px) {
      font-size: 75%;
    }
  }
  body {
    background: #f8f8f8;
    font-family: Arial, sans-serif;
  }
`;

export default GlobalStyles;
