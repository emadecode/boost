import { lightThemeColors as defaultColors } from "./colors";

// Create a theme instance.
const theme = {
  colors: {
    ...defaultColors,
  },
};

export default theme;

export type Theme = typeof theme;

export interface IThemePropsModel {
  theme?: Theme;
}
