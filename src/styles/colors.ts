export const lightThemeColors = {
  color1: "#4F5BD5",
  colorWhite: "#ffffff",
  bodyBackground: "#F8F8F8",
  colorError: "#D8000C",
};

export const darkThemeColors = {
  color1: "#4F5BD5",
  colorWhite: "#ffffff",
  bodyBackground: "#F8F8F8",
  colorError: "#D8000C",
};
