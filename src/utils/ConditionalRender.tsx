import React from "react";

interface IPropsModel {
  check: any;
  children: any;
}

const ConditionalRender: React.FC<IPropsModel> = ({ children, check }) => {
  return check ? children : "";
};

export default ConditionalRender;
