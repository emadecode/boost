import React, { FC, useState } from "react";
import styled from "styled-components";
import { Plus } from "@styled-icons/evaicons-solid/Plus";

import BoostCardButton from "../create-boost-button/BoostCardButton";
import { IThemePropsModel } from "../../styles/theme";
import BoostContent from "../boost-content/BoostContent";
import ConditionalRender from "../../utils/ConditionalRender";
import CreateBoostForm from "../create-boost-form/CreateBoostForm";

interface BoostCardHeaderStyledProps
  extends React.HTMLProps<HTMLDivElement>,
    IThemePropsModel {
  isActive?: boolean;
}

const BoostCardHeaderStyled = styled.div<BoostCardHeaderStyledProps>`
  position: relative;
  padding: 0 0.8rem;
  z-index: 0;
  transform: translateY(72%);
  transition: all 200ms ease-in-out;
  cursur: pointer;
  ${({ isActive }) =>
    isActive &&
    `
       transform: translateY(2%); 
  `}
`;

interface BoostCardBodyStyledProps
  extends React.HTMLProps<HTMLDivElement>,
    IThemePropsModel {
  isActive?: boolean;
}

const BoostCardBodyStyled = styled.div<BoostCardBodyStyledProps>`
  position: relative;
  border: 1px solid #eaeaea;
  height: 11rem;
  z-index: 1;
  background-color: #ffffff;
  border-radius: 8px;
  transition: all 200ms ease-in-out;
  ${({ isActive }) =>
    isActive &&
    `
       height: 25rem;
  `}
`;

interface BoostCardStyledProps
  extends React.HTMLProps<HTMLDivElement>,
    IThemePropsModel {}

const BoostCardStyled = styled.div<BoostCardStyledProps>`
  position: relative;
  display: inline-block;
  min-width: 20rem;
  margin-bottom: 1rem;
  flex: 1 0 auto;
  width: 100%;
  @media only screen and (min-width: 768px) {
    max-width: 50%;
  }
  @media only screen and (min-width: 1024px) {
    max-width: 25%;
  }
  padding: 0 0.5rem;
  &:hover ${BoostCardHeaderStyled} {
    transform: translateY(2%);
  }
`;

interface PlusIconStyledProps extends IThemePropsModel {
  isActive?: boolean;
}

const PlusIconStyled = styled(Plus)<PlusIconStyledProps>`
  color: ${({ theme }) => theme.colors.color1};
  width: 1.8rem;
  ${({ isActive }) =>
    isActive &&
    `
      color: white;
  `}
`;

const BoostCard: FC = () => {
  const [isAddButtonActive, setIsAddButtonActive] = useState<boolean>(false);

  return (
    <BoostCardStyled data-testid="boost-card">
      <BoostCardHeaderStyled isActive={isAddButtonActive}>
        <BoostCardButton
          isFullwidth
          onClick={() => setIsAddButtonActive(!isAddButtonActive)}
          isActive={isAddButtonActive}
        >
          <PlusIconStyled isActive={isAddButtonActive} />
        </BoostCardButton>
      </BoostCardHeaderStyled>
      <BoostCardBodyStyled isActive={isAddButtonActive}>
        <ConditionalRender check={!isAddButtonActive}>
          <BoostContent data-testid="boost-content" />
        </ConditionalRender>
        <ConditionalRender check={isAddButtonActive}>
          <CreateBoostForm
            data-testid="create-boost-form"
            onCreate={() => setIsAddButtonActive(false)}
          />
        </ConditionalRender>
      </BoostCardBodyStyled>
    </BoostCardStyled>
  );
};

export default BoostCard;
