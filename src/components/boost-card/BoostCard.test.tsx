import React from "react";
import { render, screen, fireEvent } from "../../core/lib/test-utils";

import { ThemeProvider } from "styled-components";
import { Provider as ReduxProvider } from "react-redux";
import { store } from "../../redux/store";
import theme from "../../styles/theme";
import BoostCard from "./BoostCard";

beforeEach(() => {
  render(
    <ReduxProvider store={store}>
      <ThemeProvider theme={theme}>
        <BoostCard />
      </ThemeProvider>
    </ReduxProvider>
  );
});

describe("Boost Card", () => {
  it("should show content on load", () => {
    const boostContentEl = screen.getByTestId("boost-content");
    expect(boostContentEl).toBeInTheDocument();
  });
  it("should show form on BoostCardButton click", async () => {
    const boostCardButtonEl = screen.getByTestId("boost-card-button");

    fireEvent.click(boostCardButtonEl);

    const CreateBoostFormEl = await screen.findByTestId("create-boost-form");
    expect(CreateBoostFormEl).toBeInTheDocument();
  });
});
