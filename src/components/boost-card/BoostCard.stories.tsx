import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Provider as ReduxProvider } from "react-redux";

import { store } from "../../redux/store";
import Wrapper from "../wrapper/Wrapper";

import BoostCard from "./BoostCard";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/BoostCard",
  component: BoostCard,
} as ComponentMeta<typeof BoostCard>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof BoostCard> = (args) => (
  <ReduxProvider store={store}>
    <Wrapper>
      <BoostCard {...args} />
    </Wrapper>
  </ReduxProvider>
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {};
