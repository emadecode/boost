import styled from "styled-components";

const Wrapper = styled.div`
  padding: 2rem;
  display: flex;
  flex-wrap: wrap;
  max-width: 100%;
  margin: auto;
  @media only screen and (min-width: 768px) {
    max-width: 1024px;
  }
  @media only screen and (min-width: 1024px) {
    max-width: 1366px;
  }
`;

export default Wrapper;
