import React from "react";
import styled from "styled-components";
import Label from "../Label/Label";

const BoostContentStyled = styled.div`
  padding: 2rem;
`;

const BoostContentHeaderStyled = styled.div`
  margin-bottom: 3rem;
`;

const BoostContentBodyStyled = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const BoostContentTitleStyled = styled.div`
  font-size: 1.5rem;
  text-transform: uppercase;
  font-weight: 500;
  color: #3c3c3c;
`;
const BoostContentNumberStyled = styled.div`
  font-size: 2.6rem;
  font-weight: 700;
  color: #3c3c3c;
`;

const BoostContent = ({ ...rest }) => {
  return (
    <BoostContentStyled {...rest}>
      <BoostContentHeaderStyled>
        <Label>Boost</Label>
      </BoostContentHeaderStyled>
      <BoostContentBodyStyled>
        <BoostContentTitleStyled>Liking</BoostContentTitleStyled>
        <BoostContentNumberStyled>100</BoostContentNumberStyled>
      </BoostContentBodyStyled>
    </BoostContentStyled>
  );
};

export default BoostContent;
