import styled from "styled-components";
import { IThemePropsModel } from "./../../styles/theme";

const Label = styled.span<IThemePropsModel>`
  border: 1px solid #ccd0de;
  text-transform: uppercase;
  padding: 0.4rem 1.3rem;
  border-radius: 10rem;
  color: #ccd0de;
  font-size: 1.1rem;
`;

export default Label;
