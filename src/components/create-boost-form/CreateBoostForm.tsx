import React, { FC } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { addNewCard } from "../../redux/ducks/card";
import { BoostCardModel } from "../boost-card/BoostCard.models";

const CreateBoostFormHeaderStyled = styled.div`
  text-align: center;
  margin-bottom: 2rem;
  padding: 2rem 2rem 0 2rem;
`;

const CreateBoostFormBodyStyled = styled.div`
  padding: 2rem;
  text-align: center;
`;

const CreateBoostFormSectionStyled = styled.div`
  position: relative;
  background-image: linear-gradient(
    to right,
    #a6acba 33%,
    rgba(255, 255, 255, 0) 0%
  );
  background-position: bottom;
  background-size: 9px 1px;
  background-repeat: repeat-x;
`;
const CreateBoostFormSectionTitleStyled = styled.h4`
  position: relative;
  display: inline-block;
  background: #ffffff;
  color: #a6acba;
  top: 0.7rem;
  padding: 0.2rem 1rem;
  left: 50%;
  transform: translateX(-50%);
  font-size: 1.1rem;
  font-weight: 400;
`;

const CreateBoostFormStyled = styled.div``;

const FormTitleStyled = styled.h4`
  color: #3c3c3c;
  font-size: 1.1rem;
  font-weight: 600;
`;
const DateInputContainerStyled = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  padding: 2rem 0;
`;
const DateInputTextStyled = styled.h4`
  color: rgba(60, 60, 60, 0.5);
  font-size: 1.2rem;
  font-weight: 500;
  text-transform: uppercase;
  margin-bottom: 0.5rem;
  padding: 0 1rem;
`;
const DateInputStyled = styled.h4`
  color: #3c3c3c;
  font-size: 1.3rem;
  font-weight: 600;
  padding-bottom: 0.2rem;
  border-bottom: 1px solid #3c3c3c;
`;

const SubmitButtonStyled = styled.button`
  background: #4f5bd51f;
  border: none;
  color: #4f5bd5;
  padding: 0.6rem 1.5rem;
  border-radius: 10rem;
  text-transform: uppercase;
  cursor: pointer;
`;

interface ICreateBoostFormProps {
  onCreate?: () => void;
}

const CreateBoostForm: FC<ICreateBoostFormProps> = ({ onCreate, ...rest }) => {
  const dispatch = useDispatch();

  const submitForm = () => {
    dispatch(addNewCard(new BoostCardModel()));
    onCreate?.();
  };

  return (
    <CreateBoostFormStyled {...rest}>
      <CreateBoostFormHeaderStyled>
        <SubmitButtonStyled
          onClick={submitForm}
          data-testid="create-boost-button"
        >
          Create Boost
        </SubmitButtonStyled>
      </CreateBoostFormHeaderStyled>
      <CreateBoostFormSectionStyled>
        <CreateBoostFormSectionTitleStyled>
          Date Period
        </CreateBoostFormSectionTitleStyled>
      </CreateBoostFormSectionStyled>
      <CreateBoostFormBodyStyled>
        <FormTitleStyled>Pick Date Range</FormTitleStyled>
        <DateInputContainerStyled>
          <DateInputStyled>2020/09/10</DateInputStyled>
          <DateInputTextStyled>To</DateInputTextStyled>
          <DateInputStyled>2020/09/10</DateInputStyled>
        </DateInputContainerStyled>
      </CreateBoostFormBodyStyled>
    </CreateBoostFormStyled>
  );
};

export default CreateBoostForm;
