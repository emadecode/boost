import React from "react";
import { render, screen } from "../../core/lib/test-utils";

import { ThemeProvider } from "styled-components";
import { Provider as ReduxProvider } from "react-redux";
import { store } from "../../redux/store";
import theme from "../../styles/theme";
import CreateBoostForm from "./CreateBoostForm";

beforeEach(() => {
  render(
    <ReduxProvider store={store}>
      <ThemeProvider theme={theme}>
        <CreateBoostForm />
      </ThemeProvider>
    </ReduxProvider>
  );
});

describe("Create Boost Form", () => {
  it("should show create boost button", () => {
    const createBoostButtonEl = screen.getByTestId("create-boost-button");
    expect(createBoostButtonEl).toBeInTheDocument();
  });
});
