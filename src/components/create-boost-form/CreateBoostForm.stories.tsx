import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Provider as ReduxProvider } from "react-redux";

import CreateBoostForm from "./CreateBoostForm";
import { store } from "../../redux/store";
import Wrapper from "../wrapper/Wrapper";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/CreateBoostForm",
  component: CreateBoostForm,
} as ComponentMeta<typeof CreateBoostForm>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof CreateBoostForm> = (args) => (
  <ReduxProvider store={store}>
    <Wrapper>
      <CreateBoostForm {...args} />
    </Wrapper>
  </ReduxProvider>
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {};
