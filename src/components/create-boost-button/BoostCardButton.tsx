import React, { FC } from "react";
import styled from "styled-components";
import { IThemePropsModel } from "./../../styles/theme";

interface BoostCardButtonStyledProps
  extends React.HTMLProps<HTMLButtonElement>,
    IThemePropsModel {
  onClick?: () => void;
  isFullwidth?: boolean;
  isActive?: boolean;
}

export const BoostCardButtonStyled = styled.button<BoostCardButtonStyledProps>`
  height: 3rem;
  border: none;
  background-color: #ffffff;
  border: 1px solid #eaeaea;
  padding: 0 5rem;
  width: ${({ isFullwidth = false }) => (isFullwidth ? "100%" : "auto")};
  border-radius: 8px 8px 0 0;
  cursor: pointer;
  ${({ isActive, theme }) =>
    isActive &&
    `
        background-color: ${theme.colors.color1};
        color:#ffffff;

  `}
`;

interface IBoostCardButtonProps {
  children?: React.ReactNode;
  [x: string]: any;
}

const BoostCardButton: FC<IBoostCardButtonProps> = ({ children, ...rest }) => {
  return (
    <BoostCardButtonStyled data-testid="boost-card-button" {...rest}>
      {children}
    </BoostCardButtonStyled>
  );
};

export default BoostCardButton;
