import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Plus } from "@styled-icons/evaicons-solid/Plus";

import BoostCardButton from "./BoostCardButton";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/BoostCardButton",
  component: BoostCardButton,
  subcomponents: { Plus },
} as ComponentMeta<typeof BoostCardButton>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof BoostCardButton> = (args) => (
  <BoostCardButton {...args}>
    <Plus width="25" />
  </BoostCardButton>
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  children: "+",
  isActive: false,
};

export const Active = Template.bind({});
Active.args = {
  children: "+",
  isActive: true,
};

export const Fullwidth = Template.bind({});
Fullwidth.args = {
  children: "+",
  isActive: true,
  isFullwidth: true,
};
