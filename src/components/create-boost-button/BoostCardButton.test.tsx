import React from "react";
import { render, screen } from "../../core/lib/test-utils";

import { ThemeProvider } from "styled-components";
import { Provider as ReduxProvider } from "react-redux";
import { store } from "../../redux/store";
import theme from "../../styles/theme";
import BoostCardButton from "./BoostCardButton";

beforeEach(() => {
  render(
    <ReduxProvider store={store}>
      <ThemeProvider theme={theme}>
        <BoostCardButton />
      </ThemeProvider>
    </ReduxProvider>
  );
});

describe("Boost Card Button", () => {
  it("should load correctly", () => {
    const boostCardButtonEl = screen.getByTestId("boost-card-button");
    expect(boostCardButtonEl).toBeInTheDocument();
  });
});
