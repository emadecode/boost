import { useSelector } from "react-redux";
import { ThemeProvider } from "styled-components";
import BoostCard from "./components/boost-card/BoostCard";
import Wrapper from "./components/wrapper/Wrapper";
import { RootState } from "./redux/store";
import GlobalStyles from "./styles/global";
import theme from "./styles/theme";

function App() {
  const cardsList = useSelector((state: RootState) => state.card.cardsList);

  const renderCards = () => {
    return cardsList.map((_, idx: number) => <BoostCard key={idx} />);
  };

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Wrapper>{renderCards()}</Wrapper>
    </ThemeProvider>
  );
}

export default App;
